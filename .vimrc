"----BASICS----"
set number
set relativenumber
set tabstop=2 softtabstop=2
set shiftwidth=2
set autoindent
set mouse=a
set scrolloff=10
set hidden
set incsearch
set signcolumn=yes
set colorcolumn=80
set textwidth=80
set wrap
highlight ColorColumn ctermbg=7

"-----MOVEMENT-----"
map <C-j> :m +1<Enter>
map <C-k> :m -2<Enter>

"-----COSMETICS-----"
" set status line to show the name of the current file
set statusline="%f%m%r%h%w [%Y] [0x%02.2B]%< %F%=%4v,%4l %3p%% of %L"

"-----PLUGINS-----"
call plug#begin('~/.vim/plugged')
   Plug 'ziglang/zig.vim'
   Plug 'neoclide/coc.nvim', {'branch': 'release'}
   Plug 'zah/nim.vim'
   Plug 'prabirshrestha/asyncomplete.vim'
   Plug 'prabirshrestha/async.vim'
   Plug 'prabirshrestha/vim-lsp'
   Plug 'prabirshrestha/asyncomplete-lsp.vim'
	 Plug 'tpope/vim-surround'
call plug#end()

"-----VIMRC SPECIFICS-----"
if expand("%:t")==".vimrc"
" map CTRL+C to comment a block of code
	map <C-c> :s/^/"/<Enter>
" map CTRL+V to uncomment a block of code
	map <C-u> :s/^"//<Enter>
endif

"-----C++ SPECIFICS-----"
" map f4 to switch between header and source
autocmd FileType c,cpp,h map <F4> :e %:p:s,.h$,.X123X,:s,.cpp$,.h,:s,.X123X$,.cpp,<CR>
" map CTRL+C to comment a block of code
autocmd FileType c,cpp,h map <C-c> :s/^/\/\//<Enter>
" map CTRL+V to uncomment a block of code
autocmd FileType c,cpp,h map <C-u> :s/^\/\///<Enter>
" automatically run clang format when saving buffers for cpp files
" then return to the last cursor position and center the screen
autocmd BufWritePre *.c,*.cpp,*.h norm mmgggqG`mzz

"-----NIM SPECIFICS-----"
" map CTRL+C to comment a block of code
autocmd FileType nim map <C-c> :s/^/#/<Enter>
" map CTRL+U to uncomment a block of code
autocmd FileType nim map <C-u> :s/^#//<Enter>

"---just language support---"
"The JumpToDef function hooks the nim.vim plugin to invoke the nim compiler with the appropriate idetools command. Pressing meta+g will then jump to the definition of the word your cursor is on. This uses the nim compiler instead of ctags, so it works on any nim file which is compilable without requiring you to maintain a database file.

fun! JumpToDef()
  if exists("*GotoDefinition_" . &filetype)
    call GotoDefinition_{&filetype}()
  else
    exe "norm! \<C-]>"
  endif
endf

" Jump to tag
nn <M-g> :call JumpToDef()<cr>
ino <M-g> <esc>:call JumpToDef()<cr>i

"---Nim LSP support---"
let s:nimlspexecutable = "nimlsp"
let g:lsp_log_verbose = 1
let g:lsp_log_file = expand('/tmp/vim-lsp.log')
" for asyncomplete.vim log
let g:asyncomplete_log_file = expand('/tmp/asyncomplete.log')

let g:asyncomplete_auto_popup = 0

if has('win32')
   let s:nimlspexecutable = "nimlsp.cmd"
   " Windows has no /tmp directory, but has $TEMP environment variable
   let g:lsp_log_file = expand('$TEMP/vim-lsp.log')
   let g:asyncomplete_log_file = expand('$TEMP/asyncomplete.log')
endif
if executable(s:nimlspexecutable)
   au User lsp_setup call lsp#register_server({
   \ 'name': 'nimlsp',
   \ 'cmd': {server_info->[s:nimlspexecutable]},
   \ 'whitelist': ['nim'],
   \ })
endif

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction

" 221031: this has broken tab on my new endevour OS machine
autocmd FileType .nim inoremap <silent><expr> <TAB>
  \ pumvisible() ? "\<C-n>" :
  \ <SID>check_back_space() ? "\<TAB>" :
  \ asyncomplete#force_refresh()
autocmd FileType .nim inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

"-----I CAN'T TYPE----"
" map Ex to ex to open the vim file explorer
map :ex :Ex 
" map W to w to write a buffer
map :W :w
" map Wa to wa to write all buffers
map :Wa :wa


"-----DOCUMENTATION----"
" Comment and uncomment a block of code
" Example:
"		autocmd FileType *.c,*.cpp,*.h map <C-c> :s/^/\/\//<Enter>
" Explanation:
"		:s/								/  \/\/                   / <Enter>
"			^-substitute term  ^-replace term (2 esc) ^-end of replace + Enter
